package com.example.yurii.bluetoothexoplayer.api.networking;



import com.example.yurii.bluetoothexoplayer.api.model.SendSong;
import com.example.yurii.bluetoothexoplayer.api.model.Song;
import com.example.yurii.bluetoothexoplayer.api.networking.core.Service;
import com.example.yurii.bluetoothexoplayer.api.networking.error.NetworkError;
import com.example.yurii.bluetoothexoplayer.api.service.SongApi;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class NetworkSong extends Service<SongApi> {
    public NetworkSong(SongApi networkService) {
        super(networkService);
    }

    public Disposable getSong(Callback<Song> headCallback){
        return networkService.getSong()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .onErrorResumeNext(Observable::error)
                .subscribeWith(new DisposableObserver<Song>() {


                    @Override
                    public void onError(Throwable e) {
                        headCallback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNext(Song song) {
                        headCallback.onSuccess(song);
                    }
                });
    }

    public Disposable setSong(String song,Callback<Song> headCallback){
        SendSong sendSong = new SendSong(song);
        return networkService.setSong(song)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .onErrorResumeNext(Observable::error)
                .subscribeWith(new DisposableObserver<Song>() {


                    @Override
                    public void onError(Throwable e) {
                        headCallback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNext(Song song) {
                        headCallback.onSuccess(song);
                    }
                });
    }

}
