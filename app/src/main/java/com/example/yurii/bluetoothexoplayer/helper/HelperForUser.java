package com.example.yurii.bluetoothexoplayer.helper;

import android.os.Build;
import android.widget.Toast;

import com.example.yurii.bluetoothexoplayer.R;
import com.joanfuentes.hintcase.HintCase;
import com.joanfuentes.hintcaseassets.contentholderanimators.FadeInContentHolderAnimator;
import com.joanfuentes.hintcaseassets.hintcontentholders.SimpleHintContentHolder;
import com.joanfuentes.hintcaseassets.shapeanimators.RevealRectangularShapeAnimator;
import com.joanfuentes.hintcaseassets.shapeanimators.UnrevealRectangularShapeAnimator;

import java.util.List;

public class HelperForUser {


    public static void onShowHelper(List<HelperLayout> helperLayoutList,int i) {
        HelperLayout helperLayout = helperLayoutList.get(i);
        SimpleHintContentHolder blockInfo = new SimpleHintContentHolder.Builder(helperLayout.getThView().getContext())
                .setContentTitle(helperLayout.getHelperString().getThContentTitle())
                .setContentText(helperLayout.getHelperString().getThContentText())
                .setTitleStyle(R.style.title)
                .setContentStyle(R.style.content)
                .setMarginByResourcesId(R.dimen.activity_vertical_margin,
                        R.dimen.activity_horizontal_margin,
                        R.dimen.activity_vertical_margin,
                        R.dimen.activity_horizontal_margin)
                .build();
        HintCase aCase = new HintCase(helperLayout.getThView().getRootView());
        aCase.setBackgroundColorByResourceId(R.color.colorAccent)
                .setTarget(helperLayout.getHelpView(), HintCase.TARGET_IS_NOT_CLICKABLE)
                .setShapeAnimators(new RevealRectangularShapeAnimator(), new UnrevealRectangularShapeAnimator())
                .setHintBlock(blockInfo, new FadeInContentHolderAnimator())
                .show();
        aCase.setOnClosedListener(() -> {
            if(helperLayoutList.size()!=helperLayoutList.indexOf(helperLayout)+1)
            HelperForUser.onShowHelper(helperLayoutList,helperLayoutList.indexOf(helperLayout)+1);
        });

    }
}
