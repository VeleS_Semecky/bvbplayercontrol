package com.example.yurii.bluetoothexoplayer.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Song {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("song")
    @Expose
    private String mSong;

    public Song() {
    }

    public Song(int id, String mSong) {
        this.id = id;
        this.mSong = mSong;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSong() {
        return mSong;
    }

    public void setSong(String mSong) {
        this.mSong = mSong;
    }
}