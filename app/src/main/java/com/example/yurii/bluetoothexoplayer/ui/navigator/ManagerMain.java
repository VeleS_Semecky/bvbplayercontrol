package com.example.yurii.bluetoothexoplayer.ui.navigator;


import com.example.yurii.bluetoothexoplayer.R;
import com.example.yurii.bluetoothexoplayer.ui.activity.core.BaseActivity;
import com.example.yurii.bluetoothexoplayer.ui.fragment.FirstFragment_;
import com.example.yurii.bluetoothexoplayer.ui.fragment.core.BaseFragment;
import com.example.yurii.bluetoothexoplayer.ui.navigator.core.BaseManager;
import com.example.yurii.bluetoothexoplayer.ui.navigator.core.ResourceManager;
import com.example.yurii.bluetoothexoplayer.ui.navigator.core.ResourceNames;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ManagerMain extends BaseManager {

    ManagerMain(BaseFragment baseFragment, BaseActivity baseActivity) {
        super(baseFragment, baseActivity);
    }

    @Override
    public void moveFragmentTo(int id, Object... o) {
        switch (id){
            case ResourceManager.FragmentId.FIRST_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerToolbar, FirstFragment_.builder().build(), ResourceNames.FIRST_FRAGMENT).commit();
                break;

        }
    }

    @Override
    public void removeFragment() {
            baseFragment.getActivity().getFragmentManager().beginTransaction().remove(baseFragment).commit();
    }
}
