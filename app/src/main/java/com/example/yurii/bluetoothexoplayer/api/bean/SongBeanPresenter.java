package com.example.yurii.bluetoothexoplayer.api.bean;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;
import android.widget.Toast;


import com.example.yurii.bluetoothexoplayer.api.bean.core.SongBean;
import com.example.yurii.bluetoothexoplayer.api.model.Song;
import com.example.yurii.bluetoothexoplayer.api.presenter.SongPresenter;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.FragmentByTag;
import org.androidannotations.annotations.RootContext;

@EBean

public class SongBeanPresenter extends SongBean {

    protected MutableLiveData<Song> thSongLiveData = new MutableLiveData<>();



    public MutableLiveData<Song> getThSongLiveData() {
        return thSongLiveData;
    }

    @Override
    public void init() {
        super.init();
        getSongDeps().inject(this);
    }

//    @AfterViews
//    public void getSong(){
//        new SongPresenter(networkSong,this).getSong();
//    }


    public void initGetSong(){
        new SongPresenter(networkSong,this).getSong();
    }

    public void initSetSong(String song){
        new SongPresenter(networkSong,this).setSong(song);
    }

    @Override
    public void onSuccess(Song song) {
        thSongLiveData.setValue(song);
        Toast.makeText(baseActivity, song.getSong(), Toast.LENGTH_SHORT).show();
        Log.e("GetServerData",song.getId() + " " +song.getSong());
    }

    @Override
    public void onFailed(String error) {
        Log.e("GetServerData",error);

    }
}