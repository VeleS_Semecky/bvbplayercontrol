package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageBass {


    @SerializedName("pgs")
    @Expose
    private
    int progress;

    public MessageBass() {
    }

    public MessageBass(int progress) {
        this.progress = progress;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }
}
