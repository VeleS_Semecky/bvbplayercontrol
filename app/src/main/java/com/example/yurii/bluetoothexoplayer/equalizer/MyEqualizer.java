package com.example.yurii.bluetoothexoplayer.equalizer;

import android.media.audiofx.Equalizer;


import com.example.yurii.bluetoothexoplayer.bluetooth.model.EqualizerPreset;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.SettingSeekBar;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.SettingSeekBarEqualizer;

import java.util.ArrayList;
import java.util.List;

public class MyEqualizer {
    private Equalizer mEqualizer;
    private List<SettingSeekBarEqualizer> listFrequency;
    private List<EqualizerPreset> listPresets;
    private boolean isMusicEqualizer = false;

    public Equalizer getEqualizer() {
        return mEqualizer;
    }

    public MyEqualizer() {
    }

    public MyEqualizer(int thPriority, int thAudioSessionId) {
        isMusicEqualizer = thAudioSessionId != 0;
        if(mEqualizer!=null)mEqualizer.release();
            mEqualizer = new Equalizer(thPriority, thAudioSessionId);
            mEqualizer.setEnabled(true);

    }

    public static MyEqualizer getEqualizer(MyEqualizer equalizer){
        if (equalizer.isMusicEqualizer()) {
            return equalizer;
        } else {
            equalizer.getEqualizer().release();
            return new MyEqualizer(0, 0);
        }
    }

    public List<EqualizerPreset> onListPresetsEqualizer() {
        listPresets = new ArrayList<>();
        for (short i = 0; i < mEqualizer.getNumberOfPresets(); i++) {
            listPresets.add(new EqualizerPreset(i, mEqualizer.getPresetName(i)));
        }
        return listPresets;
    }

    public short getLowerEqualizerBandLevel(){
        return mEqualizer.getBandLevelRange()[0];
    }


    public SettingSeekBar onListFrequencyEqualizer() {

        listFrequency = new ArrayList<>();
        // get number frequency bands supported by the equalizer engine
        short numberFrequencyBands = mEqualizer.getNumberOfBands();
        //        get the level ranges to be used in setting the band level
        //        get lower limit of the range in milliBels
        final short lowerEqualizerBandLevel = mEqualizer.getBandLevelRange()[0];
        //        get the upper limit of the range in millibels
        final short upperEqualizerBandLevel = mEqualizer.getBandLevelRange()[1];
        for (short i = 0; i < numberFrequencyBands; i++) {
            listFrequency.add(new SettingSeekBarEqualizer(i, mEqualizer.getCenterFreq(i), mEqualizer.getBandLevel(i) - lowerEqualizerBandLevel));
        }
        return new SettingSeekBar(lowerEqualizerBandLevel,upperEqualizerBandLevel,numberFrequencyBands,listFrequency);
    }

    public void usePresetEqualizer(short extras) {
        mEqualizer.usePreset(extras);
    }


    public void setBandLevel(short equalizerBandIndex, int progress, int lowerEqualizerBandLevel) {
        mEqualizer.setBandLevel(equalizerBandIndex,
                (short) (progress + lowerEqualizerBandLevel));
    }

    public short getBandLevel(short i){
        return mEqualizer.getBandLevel(i);
    }

    public void onDestroy() {
        if (mEqualizer != null)
            mEqualizer.release();
    }

    public List<SettingSeekBarEqualizer> getListFrequency() {
        return listFrequency;
    }

    public List<EqualizerPreset> getListPresets() {
        return listPresets;
    }

    private boolean isMusicEqualizer() {
        return isMusicEqualizer;
    }
}
