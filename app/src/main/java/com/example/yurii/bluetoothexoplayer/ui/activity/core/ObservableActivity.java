package com.example.yurii.bluetoothexoplayer.ui.activity.core;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Created by Юрий on 20.03.2018.
 */

public interface ObservableActivity {

    String ON_BACK_PRESSED = "OnBackPressed";
    List<Consumer<String>> onBackPressedListeners = new ArrayList<>();

    void setBackPressedListener(Consumer<String> onBackPressedListener);

    void removeBackPressedListener(Consumer<String> onBackPressedListener);
}
