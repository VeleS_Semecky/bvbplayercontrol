package com.example.yurii.bluetoothexoplayer.ui.navigator.core;

import android.view.View;
import android.widget.TextView;

import com.example.yurii.bluetoothexoplayer.R;
import com.example.yurii.bluetoothexoplayer.ui.activity.core.BaseActivity;
import com.example.yurii.bluetoothexoplayer.ui.fragment.core.BaseFragment;


/**
 * Created by Юрий on 24.03.2018.
 */

public abstract class BaseManager implements Manager {

    protected BaseFragment baseFragment;
    protected BaseActivity baseActivity;

    public BaseManager(BaseFragment baseFragment, BaseActivity baseActivity) {
        this.baseFragment = baseFragment;
        this.baseActivity = baseActivity;
    }

    @Override
    public void initToolbar(int id, Object... text) {
        View inflate = null;
        switch (id) {
            case ResourceManager.ToolbarId.SIMPLE:
                inflate = baseActivity.getLayoutInflater().inflate(R.layout.toolbar_simple, null,false);
//                if(text.length > 0){
//                    ((TextView)inflate.findViewById(R.id.text)).setText((CharSequence) text[0]);

                    inflate.findViewById(R.id.myLogo).setOnClickListener(v -> baseFragment.clickToolbar(R.id.myLogo));
                    inflate.findViewById(R.id.tl_bluetooth).setOnClickListener(v -> baseFragment.clickToolbar(R.id.tl_bluetooth));
                    inflate.findViewById(R.id.tl_addMusic).setOnClickListener(v -> baseFragment.clickToolbar(R.id.tl_addMusic));
//                }
                break;
//            case ResourceManager.ToolbarId.YES_OR_NO:
//                inflate = baseActivity.getLayoutInflater().inflate(R.layout.toolbar_yes_or_no, null,false);
//                inflate.findViewById(R.id.cancel_imgv).setOnClickListener(v -> baseFragment.clickToolbar(R.id.cancel_imgv));
//                inflate.findViewById(R.id.done_imgv).setOnClickListener(v -> baseFragment.clickToolbar(R.id.done_imgv));
//                break;
//            case ResourceManager.ToolbarId.PRODUCT:
//                inflate = baseActivity.getLayoutInflater().inflate(R.layout.toolbar_product, null,false);
//                if(text.length > 0){
//                    ((TextView)inflate.findViewById(R.id.txtNameShop)).setText(text[0].toString());
//                    inflate.findViewById(R.id.txtNameShop).setOnClickListener(v -> baseFragment.clickToolbar(R.id.txtNameShop));
//                }
//                inflate.findViewById(R.id.btnBack).setOnClickListener(v -> baseFragment.clickToolbar(R.id.btnBack));
//                inflate.findViewById(R.id.btnPay).setOnClickListener(v -> baseFragment.clickToolbar(R.id.btnPay));
//                break;

        }
        //add view

        baseFragment.getToolbarLayout().removeAllViews();
        baseFragment.getToolbarLayout().addView(inflate);

    }




}
