package com.example.yurii.bluetoothexoplayer.room.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;


@Entity(tableName = "equalizer_table",indices = {@Index(value = {"nameEqualizer","id"}, unique = true)})
public class EqualizerTable {
    @PrimaryKey
    private int id;

    @ColumnInfo(name = "nameEqualizer")
    private String thNameEqualizer;

    @ColumnInfo(name = "sbFirst")
    private int sbFirst;

    @ColumnInfo(name = "sbSecond")
    private int sbSecond;

    @ColumnInfo(name = "sbThird")
    private int sbThird;

    @ColumnInfo(name = "sbFourth")
    private int sbFourth;

    @ColumnInfo(name = "sbFifth")
    private int sbFifth;

    public EqualizerTable() {
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThNameEqualizer() {
        return thNameEqualizer;
    }

    public void setThNameEqualizer(String thNameEqualizer) {
        this.thNameEqualizer = thNameEqualizer;
    }

    public int getSbFirst() {
        return sbFirst;
    }

    public void setSbFirst(int sbFirst) {
        this.sbFirst = sbFirst;
    }

    public int getSbSecond() {
        return sbSecond;
    }

    public void setSbSecond(int sbSecond) {
        this.sbSecond = sbSecond;
    }

    public int getSbThird() {
        return sbThird;
    }

    public void setSbThird(int sbThird) {
        this.sbThird = sbThird;
    }

    public int getSbFourth() {
        return sbFourth;
    }

    public void setSbFourth(int sbFourth) {
        this.sbFourth = sbFourth;
    }

    public int getSbFifth() {
        return sbFifth;
    }

    public void setSbFifth(int sbFifth) {
        this.sbFifth = sbFifth;
    }
}
