package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EqualizerPreset {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String thNamePreset;

    public EqualizerPreset() {
    }

    public EqualizerPreset(int id, String thNamePreset) {
        this.id = id;
        this.thNamePreset = thNamePreset;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThNamePreset() {
        return thNamePreset;
    }

    public void setThNamePreset(String thNamePreset) {
        this.thNamePreset = thNamePreset;
    }
}
