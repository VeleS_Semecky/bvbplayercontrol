package com.example.yurii.bluetoothexoplayer.ui.activity.core;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.annimon.stream.Stream;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * Created by Юрий on 20.03.2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements ObservableActivity {
    private Observable<String> observable = Observable.just(ON_BACK_PRESSED);
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void setBackPressedListener(Consumer<String> onBackPressedListener) {
        onBackPressedListeners.add(onBackPressedListener);
    }

    @Override
    public void removeBackPressedListener(Consumer<String> onBackPressedListener) {
        onBackPressedListeners.remove(onBackPressedListener);
    }

    @Override
    public void onBackPressed() {
        if(onBackPressedListeners.size() > 1){
            callLastFragment();
        }else {
            Stream.of(onBackPressedListeners).forEach(item -> observable.subscribe(item));
        }
    }

    public void callLastFragment(){
        try {
            onBackPressedListeners.get(onBackPressedListeners.size() - 1).accept(ON_BACK_PRESSED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
