package com.example.yurii.bluetoothexoplayer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;


import com.crashlytics.android.Crashlytics;
import com.example.yurii.bluetoothexoplayer.R;
import com.example.yurii.bluetoothexoplayer.api.bean.SongBeanPresenter;
import com.example.yurii.bluetoothexoplayer.ui.activity.core.BaseActivity;
import com.example.yurii.bluetoothexoplayer.ui.navigator.NavigatorManager;
import com.example.yurii.bluetoothexoplayer.ui.navigator.core.ResourceManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.WindowFeature;

import io.fabric.sdk.android.Fabric;

@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    @Bean
    public NavigatorManager navigatorManager;
    @Bean
    SongBeanPresenter songBeanPresenter;
    @AfterViews
    public void initView(){
        Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);
        // TODO: Move this to where you establish a user session
        logUser();
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.LIST_TRACK_FRAGMENT);
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.PLAYER_CONTROL_FRAGMENT);
    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            songBeanPresenter.initSetSong(sharedText);
            Toast.makeText(this, sharedText, Toast.LENGTH_LONG).show();
            Log.i("handleSendText",sharedText);
        }
        else {
            Log.i("handleSendText","null");
        }
    }
    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier("12345");
        Crashlytics.setUserEmail("user@fabric.io");
        Crashlytics.setUserName("Test User");
    }

    private void setLink(){
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setLink();

    }
}
