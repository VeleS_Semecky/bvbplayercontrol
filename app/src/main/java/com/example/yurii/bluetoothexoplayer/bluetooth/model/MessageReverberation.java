package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageReverberation {
    @SerializedName("pgs")
    @Expose
    private
    int progress;

    public MessageReverberation() {
    }

    public MessageReverberation(int progress) {
        this.progress = progress;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }
}
