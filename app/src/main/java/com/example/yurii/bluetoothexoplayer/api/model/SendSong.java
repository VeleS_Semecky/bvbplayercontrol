package com.example.yurii.bluetoothexoplayer.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendSong {

    @SerializedName("song")
    @Expose
    private String mSong;

    public SendSong() {
    }

    public SendSong(String mSong) {
        this.mSong = mSong;
    }

    public String getSong() {
        return mSong;
    }

    public void setSong(String mSong) {
        this.mSong = mSong;
    }
}
