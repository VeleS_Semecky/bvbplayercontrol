package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingVolume {
    @SerializedName("max")
    @Expose
    private int thMaxVolume;

    @SerializedName("prg")
    @Expose
    private int thVolume;

    @SerializedName("on_off")
    @Expose
    private int thRingerMode;

    public SettingVolume() {
    }

    public SettingVolume(int thMaxVolume, int thVolume, int thRingerMode) {
        this.thMaxVolume = thMaxVolume;
        this.thVolume = thVolume;
        this.thRingerMode = thRingerMode;
    }

    public int getThMaxVolume() {
        return thMaxVolume;
    }

    public void setThMaxVolume(int thMaxVolume) {
        this.thMaxVolume = thMaxVolume;
    }

    public int getThVolume() {
        return thVolume;
    }

    public void setThVolume(int thVolume) {
        this.thVolume = thVolume;
    }

    public int getThRingerMode() {
        return thRingerMode;
    }

    public void setThRingerMode(int thRingerMode) {
        this.thRingerMode = thRingerMode;
    }
}
