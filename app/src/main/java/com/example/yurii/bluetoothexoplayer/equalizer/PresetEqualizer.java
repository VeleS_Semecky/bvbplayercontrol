package com.example.yurii.bluetoothexoplayer.equalizer;

import android.arch.persistence.room.ColumnInfo;

import java.util.List;

public enum PresetEqualizer {
    NORMAL("Normal", 1800, 1500, 1500, 1500, 1800),
    CLASSIC("Classic", 2000, 1800, 1300, 1900, 1900),
    DANCE("Dance", 2100, 1500, 1700, 1900, 1600),
    FLAT("Flat", 1500, 1500, 1500, 1500, 1500),
    FOLK("Folk", 1800, 1500, 1500, 1700, 1400),
    HEAVY_METAL("Heavy Metal", 1900, 1600, 2400, 1800, 1500),
    HIP_HOP("Hip Hop", 2000, 1800, 1500, 1600, 1800),
    JAZZ("Jazz", 1900, 1700, 1300, 1700, 2000),
    POP("Pop", 1400, 1700, 2000, 1600, 1300),
    ROCK("Rock", 2000, 1800, 1400, 1800, 2000);

    PresetEqualizer(String name, int sbFirst, int sbSecond, int sbThird, int sbFourth, int sbFifth) {
        this.name = name;
        this.sbFirst = sbFirst;
        this.sbSecond = sbSecond;
        this.sbThird = sbThird;
        this.sbFourth = sbFourth;
        this.sbFifth = sbFifth;
    }

    private String name;
    private int sbFirst;
    private int sbSecond;
    private int sbThird;
    private int sbFourth;
    private int sbFifth;

    public String getName() {
        return name;
    }

    public int getSbFirst() {
        return sbFirst;
    }

    public int getSbSecond() {
        return sbSecond;
    }

    public int getSbThird() {
        return sbThird;
    }

    public int getSbFourth() {
        return sbFourth;
    }

    public int getSbFifth() {
        return sbFifth;
    }
}
