package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeekBarEqualizer {
    @SerializedName("id")
    @Expose
    private
    int id;

    @SerializedName("pgs")
    @Expose
    private
    int progress;

    public SeekBarEqualizer() {
    }

    public SeekBarEqualizer(int id, int progress) {
        this.id = id;
        this.progress = progress;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }
}
