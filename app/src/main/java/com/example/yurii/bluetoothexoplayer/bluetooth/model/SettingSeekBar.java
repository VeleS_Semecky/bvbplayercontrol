package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SettingSeekBar {
    @SerializedName("low")
    @Expose
    private short lowerEqualizerBandLevel;

    @SerializedName("upp")
    @Expose
    private short upperEqualizerBandLevel;

    @SerializedName("nmb")
    @Expose
    private short numberFrequencyBands;

    @SerializedName("sb")
    @Expose
    private List<SettingSeekBarEqualizer> settingSeekBarEqualizers;

    public SettingSeekBar() {
    }

    public SettingSeekBar(short lowerEqualizerBandLevel, short upperEqualizerBandLevel, short numberFrequencyBands, List<SettingSeekBarEqualizer> settingSeekBarEqualizers) {
        this.lowerEqualizerBandLevel = lowerEqualizerBandLevel;
        this.upperEqualizerBandLevel = upperEqualizerBandLevel;
        this.numberFrequencyBands = numberFrequencyBands;
        this.settingSeekBarEqualizers = settingSeekBarEqualizers;
    }

    public static int getMaxSeekBar(short upperEqualizerBandLevel, short lowerEqualizerBandLevel) {
        return upperEqualizerBandLevel - lowerEqualizerBandLevel;
    }

    public static String frequencyHeaderText(int centerFrequencyBand) {
        return (centerFrequencyBand / 1000) + " Hz";
    }

    public static String lowerEqualizerBandLevelText(short lowerEqualizerBandLevel) {
        return (lowerEqualizerBandLevel / 100) + " dB";
    }

    public static String upperEqualizerBandLevelText(short upperEqualizerBandLevel) {
        return (upperEqualizerBandLevel / 100) + " dB";
    }

    public short getLowerEqualizerBandLevel() {
        return lowerEqualizerBandLevel;
    }

    public void setLowerEqualizerBandLevel(short lowerEqualizerBandLevel) {
        this.lowerEqualizerBandLevel = lowerEqualizerBandLevel;
    }

    public short getUpperEqualizerBandLevel() {
        return upperEqualizerBandLevel;
    }

    public void setUpperEqualizerBandLevel(short upperEqualizerBandLevel) {
        this.upperEqualizerBandLevel = upperEqualizerBandLevel;
    }

    public short getNumberFrequencyBands() {
        return numberFrequencyBands;
    }

    public void setNumberFrequencyBands(short numberFrequencyBands) {
        this.numberFrequencyBands = numberFrequencyBands;
    }

    public List<SettingSeekBarEqualizer> getSettingSeekBarEqualizers() {
        return settingSeekBarEqualizers;
    }

    public void setSettingSeekBarEqualizers(List<SettingSeekBarEqualizer> settingSeekBarEqualizers) {
        this.settingSeekBarEqualizers = settingSeekBarEqualizers;
    }
}
