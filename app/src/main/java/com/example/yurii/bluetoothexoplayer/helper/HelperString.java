package com.example.yurii.bluetoothexoplayer.helper;

public class HelperString {
    private String thContentTitle;
    private String thContentText;

    public HelperString() {
    }

    public HelperString(String thContentTitle, String thContentText) {
        this.thContentTitle = thContentTitle;
        this.thContentText = thContentText;
    }

    public String getThContentTitle() {
        return thContentTitle;
    }

    public void setThContentTitle(String thContentTitle) {
        this.thContentTitle = thContentTitle;
    }

    public String getThContentText() {
        return thContentText;
    }

    public void setThContentText(String thContentText) {
        this.thContentText = thContentText;
    }
}
