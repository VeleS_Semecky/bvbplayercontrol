package com.example.yurii.bluetoothexoplayer.equalizer;

import android.media.audiofx.BassBoost;
import android.util.Log;

public class MyBass {
    private BassBoost mBass;
    private boolean isMusicBass = false;
    private final static short MAX = 1000;

    public MyBass(int thPriority, int thAudioSessionId) {
        isMusicBass = thAudioSessionId != 0;
        if(mBass!=null)mBass.release();
        mBass = new BassBoost(thPriority, thAudioSessionId);
        mBass.setEnabled(true);
    }

    private short getStrength() {
        return mBass.getStrengthSupported() ?mBass.getRoundedStrength():-1;
    }

    private void setStrength(short strength){

        if(mBass.getStrengthSupported()) {
            mBass.setStrength(strength);
        }else {
            Log.e("BassSetting","BassNotStrengthSupported");
        }
    }

    public static MyBass getMyBass(MyBass bass){
        if (bass.isMusicBass()) {
            return bass;
        } else {
            bass.getBass().release();
            return new MyBass(0, 0);
        }
    }

    private boolean isMusicBass() {
        return isMusicBass;
    }

    public BassBoost getBass() {
        return mBass;
    }

    public void setmBass(BassBoost mBass) {
        this.mBass = mBass;
    }

    public void onDestroy() {
        if (mBass != null)
            mBass.release();
    }

}
