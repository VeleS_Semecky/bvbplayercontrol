package com.example.yurii.bluetoothexoplayer.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;

/**
 * This thread runs while listening for incoming connections. It behaves
 * like a server-side client. It runs until a connection is accepted
 * (or until cancelled).
 */
public class BluetoothAcceptThread extends Thread {
    // The local server socket
    private final BluetoothServerSocket mmServerSocket;
    private final BluetoothModuleController bluetoothModuleController;
    private static final String TAG = "BluetoothAcceptThread";

    // Name for the SDP record when creating server socket
    public BluetoothAcceptThread(BluetoothAdapter mAdapter, BluetoothModuleController bluetoothModuleController) {
        this.bluetoothModuleController = bluetoothModuleController;
        BluetoothServerSocket tmp = null;
        // Create a new listening server socket
        try {
            tmp = mAdapter.listenUsingRfcommWithServiceRecord(BluetoothModuleController.NAME, BluetoothModuleController.MY_UUID);
        } catch (IOException e) {
            Log.e(TAG, "BluetoothAcceptThread", e.fillInStackTrace());

        }
        mmServerSocket = tmp;
    }

    public void run() {
        setName("BluetoothAcceptThread");
        BluetoothSocket socket = null;
        // Listen to the server socket if we're not connected
        while (bluetoothModuleController.getState() != BluetoothModuleController.STATE_CONNECTED) {
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                assert mmServerSocket != null;
                socket = mmServerSocket.accept();
            } catch (IOException e) {
                Log.e(TAG, "BluetoothAcceptThreadIOException", e.fillInStackTrace());
                break;
            } catch (NullPointerException e) {
                Log.e(TAG, "BluetoothAcceptThreadNullPointerException", e.fillInStackTrace());
                break;
            }
            // If a connection was accepted
            if (socket != null) {
                synchronized (bluetoothModuleController) {
                    switch (bluetoothModuleController.getState()) {
                        case BluetoothModuleController.STATE_LISTEN:
                        case BluetoothModuleController.STATE_CONNECTING:
                            // Situation normal. Start the connected thread.
                            bluetoothModuleController.connected(socket, socket.getRemoteDevice());
                            break;
                        case BluetoothModuleController.STATE_NONE:
                        case BluetoothModuleController.STATE_CONNECTED:
                            // Either not ready or already connected. Terminate new socket.
                            try {
                                socket.close();
                            } catch (IOException e) {
                                Log.e(TAG, "run", e.fillInStackTrace());

                            }
                            break;
                    }
                }
            }
        }
    }

    public void cancel() {
        try {
            mmServerSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "IOExceptionCancel", e.fillInStackTrace());
        }catch (NullPointerException e){
            Log.e(TAG, "NullPointerExceptionCancel", e.fillInStackTrace());

        }
    }
}