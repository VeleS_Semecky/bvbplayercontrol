package com.example.yurii.bluetoothexoplayer.api.core;

public interface   BaseView<T> {
    void onFailed(String error);

    void onSuccess(T t);

    void showWait();
    void closeWait();
}
