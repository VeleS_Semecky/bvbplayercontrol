package com.example.yurii.bluetoothexoplayer.equalizer;

import android.media.audiofx.PresetReverb;
import android.util.Log;

public class MyReverberation {
    private PresetReverb mReverberation;
    private boolean isMusicBass;

    public MyReverberation(int thPriority, int thAudioSessionId) {
        isMusicBass = thAudioSessionId != 0;
        if(mReverberation!=null)mReverberation.release();
        mReverberation = new PresetReverb(thPriority, thAudioSessionId);
        mReverberation.setEnabled(true);
        Log.i("BassSetting", String.valueOf(mReverberation.getPreset()));
    }

    public short getPreset() {
        return mReverberation.getPreset();
    }

    public void setPreset(short strength){
        mReverberation.setPreset(strength);
        Log.i("BassSetting",strength + "SetStrength");
    }

    public static MyReverberation getMyReverberation(MyReverberation mReverberation){
        if (mReverberation.isMusicReverberation()) {
            Log.i("BassSetting","true");
            return mReverberation;
        } else {
            Log.i("BassSetting","false");
            mReverberation.getReverberation().release();
            return new MyReverberation(0, 0);
        }
    }

    private boolean isMusicReverberation() {
        return isMusicBass;
    }

    public PresetReverb getReverberation() {
        return mReverberation;
    }

    public void setReverberation(PresetReverb mReverberation) {
        this.mReverberation = mReverberation;
    }

    public void onDestroy() {
        if (mReverberation != null)
            mReverberation.release();
    }

}

