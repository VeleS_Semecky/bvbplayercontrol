package com.example.yurii.bluetoothexoplayer.ui.navigator.core;

/**
 * Created by Юрий on 24.03.2018.
 */

public interface Manager {

    void moveFragmentTo(int id, Object... o);

    void removeFragment();

    void initToolbar(int id, Object... text);

}
