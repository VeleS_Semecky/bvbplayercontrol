package com.example.yurii.bluetoothexoplayer.api.deps;



import com.example.yurii.bluetoothexoplayer.api.bean.SongBeanPresenter;
import com.example.yurii.bluetoothexoplayer.api.networking.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class})
public interface SongDeps  {

    void inject(SongBeanPresenter songBeanPresenter);


}