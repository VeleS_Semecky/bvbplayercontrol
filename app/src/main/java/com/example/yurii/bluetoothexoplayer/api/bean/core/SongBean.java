package com.example.yurii.bluetoothexoplayer.api.bean.core;



import com.example.yurii.bluetoothexoplayer.api.deps.DaggerSongDeps;
import com.example.yurii.bluetoothexoplayer.api.deps.SongDeps;
import com.example.yurii.bluetoothexoplayer.api.model.Song;
import com.example.yurii.bluetoothexoplayer.api.networking.NetworkSong;
import com.example.yurii.bluetoothexoplayer.api.networking.module.NetworkModule;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;

import java.io.File;

import javax.inject.Inject;

@EBean
public class SongBean extends Bean<Song> {
    private SongDeps songDeps;

    public SongDeps getSongDeps() {
        return songDeps;
    }

    @Inject
    public NetworkSong networkSong;


    @AfterInject
    public void init(){
        songDeps = DaggerSongDeps.builder().networkModule(

                new NetworkModule(
                        new File(
                                baseActivity.getCacheDir(),
                                "responses"),"https://class-music.herokuapp.com")).build();
    }
}