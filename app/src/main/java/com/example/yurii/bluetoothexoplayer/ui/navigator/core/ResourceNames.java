package com.example.yurii.bluetoothexoplayer.ui.navigator.core;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ResourceNames {

    public static final String LIST_TRACK_FRAGMENT = "ListTrackFragment";
    public static final String PLAYER_TRACK_FRAGMENT = "PlayerTrackFragment";
    public static final String PLAYER_CONTROL_FRAGMENT = "PlayerControlFragment";
    public static final String FIRST_FRAGMENT = "FirstFragment";



}
