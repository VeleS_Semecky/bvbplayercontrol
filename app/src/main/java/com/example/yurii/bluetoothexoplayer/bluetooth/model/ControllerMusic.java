package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ControllerMusic {
    @SerializedName("prg")
    @Expose
    private int thVolume;

    public ControllerMusic() {
    }

    public ControllerMusic(int thVolume) {
        this.thVolume = thVolume;
    }

    public int getThVolume() {
        return thVolume;
    }

    public void setThVolume(int thVolume) {
        this.thVolume = thVolume;
    }
}
