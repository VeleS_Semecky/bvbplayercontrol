package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ButtonMusicOnOrOff {
    @SerializedName("on_off")
    @Expose
    private int thRingerMode;

    public ButtonMusicOnOrOff() {
    }

    public ButtonMusicOnOrOff(int thRingerMode) {
        this.thRingerMode = thRingerMode;
    }

    public int getThRingerMode() {
        return thRingerMode;
    }

    public void setThRingerMode(int thRingerMode) {
        this.thRingerMode = thRingerMode;
    }
}
