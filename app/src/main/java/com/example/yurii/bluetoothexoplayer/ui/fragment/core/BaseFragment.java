package com.example.yurii.bluetoothexoplayer.ui.fragment.core;

import android.app.Activity;
import android.app.Fragment;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.yurii.bluetoothexoplayer.R;
import com.example.yurii.bluetoothexoplayer.ui.activity.core.BaseActivity;
import com.example.yurii.bluetoothexoplayer.ui.activity.core.OnBackPressedListener;
import com.example.yurii.bluetoothexoplayer.ui.navigator.NavigatorManager;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import io.reactivex.functions.Consumer;

/**
 * Created by Юрий on 24.03.2018.
 */
@EFragment
public abstract class BaseFragment extends Fragment implements OnBackPressedListener {

    @ViewById(R.id.toolbar)
    protected Toolbar toolbar;

    @ViewById(R.id.toolbarContainer)
    protected RelativeLayout toolbarLayout;

    public RelativeLayout getToolbarLayout() {
        return toolbarLayout;
    }

    private Consumer<String> onNextAction = s ->backPressed();


    @Override
    public void backPressed() {
        getActivity().finish();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onAttachToActivity();
    }

    protected void onAttachToActivity(){
        getBaseActivity().setBackPressedListener(onNextAction);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onAttachToActivity();
    }

    protected BaseActivity getBaseActivity(){
        return (BaseActivity) getActivity();
    }

    @Override
    public void onDetach() {
        getBaseActivity().removeBackPressedListener(onNextAction);
        super.onDetach();
    }

    @Bean
    protected NavigatorManager navigatorManager;

    public void clickToolbar(int id){
        Toast.makeText(getActivity(),R.string.app_name,Toast.LENGTH_SHORT).show();
    }

    static class ViewLifecycleOwner implements LifecycleOwner {
        private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

        @Override
        public LifecycleRegistry getLifecycle() {
            return lifecycleRegistry;
        }
    }
    @Nullable
    private ViewLifecycleOwner viewLifecycleOwner;

    /**
     * @return the Lifecycle owner of the current view hierarchy,
     * or null if there is no current view hierarchy.
     */
    public LifecycleOwner getViewLifecycleOwner() {
        return viewLifecycleOwner;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewLifecycleOwner = new ViewLifecycleOwner();
        viewLifecycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (viewLifecycleOwner != null) {
            viewLifecycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_START);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (viewLifecycleOwner != null) {
            viewLifecycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
        }
    }

    @Override
    public void onPause() {
        if (viewLifecycleOwner != null) {
            viewLifecycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_PAUSE);
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (viewLifecycleOwner != null) {
            viewLifecycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_STOP);
        }
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        if (viewLifecycleOwner != null) {
            viewLifecycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_DESTROY);
            viewLifecycleOwner = null;
        }
        super.onDestroyView();
    }
}

