package com.example.yurii.bluetoothexoplayer.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;


import com.example.yurii.bluetoothexoplayer.room.model.EqualizerTable;

import java.util.List;

@Dao
public interface EqualizerDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllSettingEqualizer(List<EqualizerTable> equalizerTables);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertSettingEqualizer(EqualizerTable equalizerTable);

    @Delete
    void delete(EqualizerTable equalizerTable);


    @Query("SELECT * FROM equalizer_table")
    LiveData<List<EqualizerTable>> getAllSettingEqualizer();

    @Query("SELECT * FROM equalizer_table WHERE id=:id")
    LiveData<EqualizerTable> getEqualizerSettingWithID(int id);

    @Query("SELECT * FROM equalizer_table WHERE nameEqualizer=:nameEqualizer")
    LiveData<EqualizerTable> getEqualizerSettingWithNameEqualizer(String nameEqualizer);
}
