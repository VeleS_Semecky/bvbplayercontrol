package com.example.yurii.bluetoothexoplayer.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yurii.bluetoothexoplayer.R;
import com.example.yurii.bluetoothexoplayer.api.bean.SongBeanPresenter;
import com.example.yurii.bluetoothexoplayer.bluetooth.BluetoothModuleController;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.ButtonMusicOnOrOff;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.ControllerMusic;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.EqualizerPreset;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.MessageBass;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.MessageBluetooth;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.MessageReverberation;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.SeekBarEqualizer;
import com.example.yurii.bluetoothexoplayer.bluetooth.model.SettingSeekBar;
import com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer;
import com.example.yurii.bluetoothexoplayer.helper.HelperForUser;
import com.example.yurii.bluetoothexoplayer.helper.HelperLayout;
import com.example.yurii.bluetoothexoplayer.helper.HelperString;
import com.example.yurii.bluetoothexoplayer.room.RoomViewModel;
import com.example.yurii.bluetoothexoplayer.room.model.EqualizerTable;
import com.example.yurii.bluetoothexoplayer.ui.fragment.core.BaseFragment;
import com.example.yurii.bluetoothexoplayer.ui.navigator.core.ResourceManager;
import com.google.gson.Gson;
import com.sdsmdg.harjot.crollerTest.Croller;
import com.sdsmdg.harjot.crollerTest.OnCrollerChangeListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.CLASSIC;
import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.DANCE;
import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.FLAT;
import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.FOLK;
import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.HEAVY_METAL;
import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.HIP_HOP;
import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.JAZZ;
import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.NORMAL;
import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.POP;
import static com.example.yurii.bluetoothexoplayer.equalizer.PresetEqualizer.ROCK;

/**
 * Created by Юрий on 24.03.2018.
 */
@EFragment(R.layout.fragment_player)
public class FirstFragment extends BaseFragment implements OnCrollerChangeListener, SeekBar.OnSeekBarChangeListener, Spinner.OnItemSelectedListener {


    // Intent request codes
    private static final int REQUEST_ENABLE_BT = 2;


    private ArrayList<BluetoothDevice> bluetoothDevices = new ArrayList<>();

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private BluetoothModuleController mModuleControllerBluetooth = null;
    // Name of the connected device
    private String mConnectedDeviceName = null;
    private int fabMuteOrUnMute = 2;
    private Gson gson = new Gson();
    private MessageBluetooth msgBL;

    private AlertDialog alert;

    private ArrayList<String> equalizerPresetNames;
    private RoomViewModel roomViewModel;

    private int thCrollerChange = 0;

    private ArrayAdapter<String> equalizerPresetSpinnerAdapter;

    @ViewById(R.id.bluetooth)
    protected ImageView bluetooth;

    @ViewById(R.id.fabMusicOffOrOn)
    protected FloatingActionButton vlMuteOrUnMute;

    @ViewById(R.id.croller)
    protected Croller croller;

    @ViewsById({R.id.sbFirstDecibel, R.id.sbSecondDecibel, R.id.sbThirdDecibel, R.id.sbFourthDecibel, R.id.sbFifthDecibel})
    List<SeekBar> seekBarEqualizers;

    @ViewById(R.id.sbBass)
    SeekBar sbBass;

    @ViewById(R.id.tvBass)
    TextView tvBass;

    @ViewById(R.id.sbReverberation)
    SeekBar sbReverberation;

    @ViewById(R.id.tvReverberation)
    TextView tvReverberation;

    @ViewsById({R.id.tvFirstDecibel, R.id.tvSecondDecibel, R.id.tvThirdDecibel, R.id.tvFourthDecibel, R.id.tvFifthDecibel})
    List<TextView> txvDecibel;

    @ViewsById({R.id.tvFirstHz, R.id.tvSecondHz, R.id.tvThirdHz, R.id.tvFourthHz, R.id.tvFifthHz})
    List<TextView> txvHz;

    @ViewById(R.id.equalizerContainer)
    LinearLayout equalizerContainer;

    @ViewById(R.id.clReverberation)
    ConstraintLayout clReverberation;

    @ViewById(R.id.clBass)
    ConstraintLayout clBass;

    @ViewById(R.id.clVolumeControl)
    ConstraintLayout clVolumeControl;
    @ViewById(R.id.clSpinner)
    ConstraintLayout clSpinner;

    @ViewById(R.id.tl_bluetooth)
    protected ImageView tl_bluetooth;

//    @Bean
//    SongBeanPresenter songBeanPresenter;
//
//    private void handleSendText(Intent intent) {
//        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
//        if (sharedText != null) {
//            songBeanPresenter.initSetSong(sharedText);
//            Toast.makeText(getBaseActivity(), sharedText, Toast.LENGTH_LONG).show();
//            Log.i("handleSendText",sharedText);
//        }
//    }
//    private void setLink(){
//        Intent intent = getBaseActivity().getIntent();
//        Bundle bundle = intent.getExtras();
//        if(bundle!=null)
//        for (String key : bundle.keySet()) {
//            Log.d("KEY", key);
//        }
//        String subject = getBaseActivity().getIntent().getStringExtra(Intent.EXTRA_SUBJECT);
//        String text = getBaseActivity().getIntent().getStringExtra(Intent.EXTRA_TEXT);
//        String action = intent.getAction();
//        String type = intent.getType();
//        Uri data = intent.getData();
//
//        Log.i("handleSendText","hui"+ data+" "+action+" "+type+" "+subject+ " " + text);
//
////        if (Intent.ACTION_SEND.equals(action) && type != null) {
////            if ("text/plain".equals(type)) {
//        handleSendText(intent); // Handle text being sent
////            }
////        }
//    }
    @AfterViews
    protected void initSVM() {
//        setLink();
        roomViewModel = ViewModelProviders.of(getBaseActivity()).get(RoomViewModel.class);
        navigatorManager.getMainManager(this).initToolbar(ResourceManager.ToolbarId.SIMPLE, getBaseActivity().getResources().getText(R.string.app_name));
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(getBaseActivity(), "Bluetooth is not available", Toast.LENGTH_LONG).show();
        }
//        if (mBluetoothAdapter.isEnabled()) {
//            if (mModuleControllerBluetooth == null) onStartBluetooth();
//            blthAddPairedDevices();
//        }
        onSetItemSpinner();

        croller.setOnCrollerChangeListener(this);
        onSetSeekBarEqualizerChangeListener();
        onSetSeekBarBassAndReverberationChangeListener();
        initRoom();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
    }

    private void initRoom() {
        roomViewModel.getRepository().getAllSettingEqualizer().observe(getViewLifecycleOwner(), equalizerTables -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                assert equalizerTables != null;
                equalizerTables.forEach(equalizerTable -> Log.i("RoomViewModel", equalizerTable.getId() + " "
                        + equalizerTable.getThNameEqualizer() + " "
                        + equalizerTable.getSbFirst() + " "
                        + equalizerTable.getSbSecond() + " "
                        + equalizerTable.getSbThird() + " "
                        + equalizerTable.getSbFourth() + " "
                        + equalizerTable.getSbFifth() + " end"
                ));
            }

        });
    }

    @Click(R.id.fabMusicOffOrOn)
    protected void vlMuteOrUnMute(FloatingActionButton fab) {
        switch (fabMuteOrUnMute) {
            case 0:
                msgBL = new MessageBluetooth();
                msgBL.setMusicOnOrOff(new ButtonMusicOnOrOff(fabMuteOrUnMute));
                vlMuteOrUnMute.setImageResource(R.drawable.ic_volume_up_black_24dp);
                sendMessage(msgBL);
                fabMuteOrUnMute = 2;
                break;
            case 2:
                msgBL = new MessageBluetooth();
                msgBL.setMusicOnOrOff(new ButtonMusicOnOrOff(fabMuteOrUnMute));
                vlMuteOrUnMute.setImageResource(R.drawable.ic_volume_off_black_24dp);
                sendMessage(msgBL);
                fabMuteOrUnMute = 0;
                break;
        }
    }


    private List<HelperLayout> getHelper() {
        List<HelperLayout> helperLayoutList = new ArrayList<>();
        helperLayoutList.add(new HelperLayout(getToolbarLayout(),getToolbarLayout().findViewById(R.id.tl_bluetooth),
                new HelperString(getString(R.string.help_title_bluetooth),getString(R.string.help_text_bluetooth))));
        helperLayoutList.add(new HelperLayout(getToolbarLayout(),getToolbarLayout().findViewById(R.id.tl_addMusic),
                new HelperString(getString(R.string.help_title_ddMusic),getString(R.string.help_text_ddMusic))));
        helperLayoutList.add(new HelperLayout(getToolbarLayout(),getToolbarLayout().findViewById(R.id.myLogo),
                new HelperString(getString(R.string.help_title_myLogo),getString(R.string.help_text_myLogo))));
        helperLayoutList.add(new HelperLayout(getView(), clVolumeControl,
                new HelperString(getString(R.string.help_title_volume),getString(R.string.help_text_volume))));
        helperLayoutList.add(new HelperLayout(getView(), clReverberation,
                new HelperString(getString(R.string.help_title_reverberatio),getString(R.string.help_text_reverberatio))));
        helperLayoutList.add(new HelperLayout(getView(), clBass,
                new HelperString(getString(R.string.help_title_bass),getString(R.string.help_text_bass))));
        helperLayoutList.add(new HelperLayout(getView(), equalizerContainer,
                new HelperString(getString(R.string.help_title_equalizer),getString(R.string.help_text_equalizer))));
        helperLayoutList.add(new HelperLayout(getView(), clSpinner,
                new HelperString(getString(R.string.help_title_preset),getString(R.string.help_text_preset))));
        return helperLayoutList;
    }

    @Override
    public void clickToolbar(int id) {
        switch (id) {
            case R.id.tl_bluetooth:
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                } else {
                    dlgItemBluetoothPairedDevices();
                }
                break;
            case R.id.tl_addMusic:
                Intent launchIntent = getBaseActivity().getPackageManager().getLaunchIntentForPackage("com.google.android.youtube");
                startActivity(launchIntent);
                getBaseActivity().finish();
                break;
            case R.id.myLogo:
                HelperForUser.onShowHelper(getHelper(),0);
                break;
            default:
                super.clickToolbar(id);
                break;
        }
    }

    @Override
    public void onDetach() {
        if (mModuleControllerBluetooth != null) mModuleControllerBluetooth.stop();
        if (alert.isShowing()) alert.dismiss();
        super.onDetach();
    }


    private void onStartBluetooth() {
        Log.i("mBluetoothAddress", thUUID(mBluetoothAdapter.getName()));
        mModuleControllerBluetooth = new BluetoothModuleController(mHandler, thUUID(mBluetoothAdapter.getName()));
        if (mModuleControllerBluetooth.getState() == BluetoothModuleController.STATE_NONE) {
            mModuleControllerBluetooth.start();
        }

    }

    private String thUUID(String name) {
        String UUID;
        StringBuilder thAddress = new StringBuilder();
        Matcher match = Pattern.compile("[0-9]|[a-zA-Z]").matcher(name);
        while (match.find()) {
            thAddress.append(match.group());
        }
        if (thAddress.length() > 6) {
            thAddress = new StringBuilder(thAddress.substring(0, 6));
        } else {
            thAddress.insert(0, "080020".substring(0, 6 - thAddress.length()));
        }
        byte[] b = thAddress.toString().getBytes(Charset.forName("UTF-8"));
        StringBuilder UUIDBuilder = new StringBuilder();
        for (byte w :
                b) {
            UUIDBuilder.append(String.valueOf(w));
        }
        UUID = UUIDBuilder.toString().substring(0, 12);
        Log.i("UUID.toLowerCase()", UUID);
        return "fa87c0d0-afac-11de-8a39-" + UUID;
    }

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothModuleController.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    break;
                case BluetoothModuleController.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
//                    Toast.makeText(
//                            getBaseActivity(), readMessage + " " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    Log.i("HandlerLeak", readMessage + " " + mConnectedDeviceName);
                    msgBL = gson.fromJson(readMessage, MessageBluetooth.class);
                    setVolume(msgBL);
                    onSetItemSpinner(msgBL);
                    onSetSeekBarEqualizerSpinner(msgBL);
                    onSetSeekBarBassAndReverberation(msgBL);
                    onSetRoomSettingEqualizer(msgBL);
                    break;
                case BluetoothModuleController.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(BluetoothModuleController.DEVICE_NAME);
                    Toast.makeText(getBaseActivity(), "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case BluetoothModuleController.MESSAGE_TOAST:
                    Toast.makeText(getBaseActivity(), msg.getData().getString(BluetoothModuleController.TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
                case BluetoothModuleController.MESSAGE_LOST:
                    mModuleControllerBluetooth.start();

                    Toast.makeText(getBaseActivity(), msg.getData().getString(BluetoothModuleController.TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void onSetSeekBarBassAndReverberation(MessageBluetooth msg) {

        if (msg.getMessageBass() != null) {
            sbBass.setProgress(msg.getMessageBass().getProgress());
            String text = msg.getMessageBass().getProgress() / 10 + "%";
            tvBass.setText(text);
        }
        if (msg.getMessageReverberation() != null) {
            sbReverberation.setProgress(msg.getMessageReverberation().getProgress() * 100);
            String text = msg.getMessageReverberation().getProgress() * 100 / 6 + "%";
            tvReverberation.setText(text);
        }
    }

    private void dlgItemBluetoothPairedDevices() {
        String[] items = new String[bluetoothDevices.size()];
        for (int i = 0; i < bluetoothDevices.size(); i++) {
            items[i] = bluetoothDevices.get(i).getName() + ": " + bluetoothDevices.get(i).getAddress();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getBaseActivity());
        builder.setTitle("Choose Bluetooth:");
        builder.setItems(items, (dialog, which) -> {
            dialog.dismiss();
            if (which >= 0 && which < bluetoothDevices.size()) {
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice("94:65:2D:3F:80:56");
                // Attempt to connect to the device
                if (mModuleControllerBluetooth.getState() == BluetoothModuleController.STATE_NONE) {
                    mModuleControllerBluetooth.start();
                }
                mModuleControllerBluetooth.connect(bluetoothDevices.get(which));

            }
        });
        alert = builder.create();
        alert.show();
    }

    private void blthAddPairedDevices() {
        // Get the local Bluetooth adapter
        BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

        // If there are paired devices, add each one to the ArrayAdapter
        if (pairedDevices.size() > 0) {
            bluetoothDevices.addAll(pairedDevices);
        } else {
            String noDevices = getResources().getText(R.string.none_paired).toString();
//            Toast.makeText(getBaseActivity(), noDevices, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
//        setLink();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    if (mModuleControllerBluetooth == null) onStartBluetooth();
                    if (bluetoothDevices.size() == 0) blthAddPairedDevices();

                } else {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(getBaseActivity(), "You did not enable Bluetooth", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private int sbCroller;
    private int i;

    @Override
    public void onProgressChanged(Croller croller, int progress) {
        int delete = thCrollerChange == 0 ? 20 : thCrollerChange;
        if (sbCroller != progress && ((sbCroller - progress) >= delete || (sbCroller - progress) <= -delete)) {
            msgBL = new MessageBluetooth();
            msgBL.setControllerMusic(new ControllerMusic(croller.getProgress() / 100));
            Log.i("CrollerStop", String.valueOf(croller.getProgress()));
            sendMessage(msgBL);
            Log.i("onProgressChanged", "onProgressChanged: " + sbCroller + " " + progress);
            sbCroller = progress;
        }
        Log.i("CrollerProgress", String.valueOf(progress) + " " + String.valueOf(croller.getProgress()));
    }

    @Override
    public void onStartTrackingTouch(Croller croller) {
        i = 0;
        sbCroller = croller.getProgress();
        Log.i("CrollerStart", String.valueOf(croller.getProgress()));
//        Toast.makeText(getBaseActivity(), "Start", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStopTrackingTouch(Croller croller) {
        i = 0;
        msgBL = new MessageBluetooth();
        msgBL.setControllerMusic(new ControllerMusic(croller.getProgress() / 100));
        Log.i("CrollerStop", String.valueOf(croller.getProgress()));
        sendMessage(msgBL);
//        Toast.makeText(getBaseActivity(), "Stop", Toast.LENGTH_SHORT).show();
    }

    private void setVolume(MessageBluetooth msg) {
        if (msg.getSettingsVolume() != null) {
            thCrollerChange = msg.getSettingsVolume().getThMaxVolume() * 100 / 5;
            Log.i("thCrollerChange", msg.getSettingsVolume().getThMaxVolume() + " " + msg.getSettingsVolume().getThMaxVolume() * 100 / 5);

            croller.setMax(msg.getSettingsVolume().getThMaxVolume() * 100);
            croller.setProgress(msg.getSettingsVolume().getThVolume() * 100);
            switch (msg.getSettingsVolume().getThRingerMode()) {
                case 0:
                    vlMuteOrUnMute.setImageResource(R.drawable.ic_volume_off_black_24dp);
                    fabMuteOrUnMute = 0;
                    break;
                case 2:
                    vlMuteOrUnMute.setImageResource(R.drawable.ic_volume_up_black_24dp);
                    fabMuteOrUnMute = 2;
                    break;
            }
        }
    }


    private void onSetSeekBarEqualizerChangeListener() {
        for (SeekBar sB : seekBarEqualizers) {
            sB.setMax(3000);
            sB.setProgress(1500);
            sB.setOnSeekBarChangeListener(this);
        }
    }

    private void onSetSeekBarBassAndReverberationChangeListener() {
        sbBass.setOnSeekBarChangeListener(this);
        sbReverberation.setOnSeekBarChangeListener(this);
        sbBass.setMax(1000);
        sbReverberation.setMax(600);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        String percentage;
        if (seekBar.getId() == sbBass.getId()) {
            percentage = seekBar.getProgress() / 10 + getString(R.string.percentage);
            tvBass.setText(percentage);
            return;
        }
        if (seekBar == sbReverberation) {
            percentage = seekBar.getProgress() / 6 + getString(R.string.percentage);
            tvReverberation.setText(percentage);
            return;
        }
        for (SeekBar sb : seekBarEqualizers) {
            if (sb.getId() == seekBar.getId()) {
                String text = String.valueOf(seekBar.getProgress() - 1500) + getString(R.string.db);
                String i = seekBar.getProgress() > 1500 ? "+" + text : text;
                txvDecibel.get(seekBarEqualizers.indexOf(sb)).setText(i);
                return;
//                Toast.makeText(getBaseActivity(), seekBar.getProgress()+" dp", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (seekBar.getId() == sbBass.getId()) {
            msgBL = new MessageBluetooth();
            msgBL.setMessageBass(new MessageBass(seekBar.getProgress()));
            sendMessage(msgBL);
            return;
        }
        if (seekBar == sbReverberation) {
            Log.i("Reverberation", seekBar.getProgress() / 100 + " Number");
            msgBL = new MessageBluetooth();
            msgBL.setMessageReverberation(new MessageReverberation(seekBar.getProgress() / 100));
            sendMessage(msgBL);
            return;
        }
        for (SeekBar sb : seekBarEqualizers) {

            thSpinner.setSelection(0);
            if (sb.getId() == seekBar.getId()) {
                msgBL = new MessageBluetooth();
                msgBL.setSeekBarEqualizer(new SeekBarEqualizer(seekBarEqualizers.indexOf(sb), sb.getProgress()));
                sendMessage(msgBL);
                return;
            }
        }

    }

    @ViewById(R.id.equalizer_preset_spinner)
    Spinner thSpinner;

    private void onSetItemSpinner(MessageBluetooth msg) {
        if (msg.getSettingsPreset() != null) {
            if (equalizerPresetNames != null) equalizerPresetNames.clear();
            equalizerPresetNames = new ArrayList<>();
            equalizerPresetNames.add("Custom");
            for (EqualizerPreset ep : msg.getSettingsPreset()) {
                equalizerPresetNames.add(ep.getThNamePreset());
            }
            equalizerPresetSpinnerAdapter = new ArrayAdapter<>(getBaseActivity(),
                    R.layout.spinner_item,
                    equalizerPresetNames);
            equalizerPresetSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            thSpinner.setAdapter(equalizerPresetSpinnerAdapter);
            thSpinner.setOnItemSelectedListener(this);
        }
    }

    private void onSetItemSpinner() {
        if (equalizerPresetNames != null) equalizerPresetNames.clear();
        equalizerPresetNames = new ArrayList<>();
        equalizerPresetNames.add("Custom");
        equalizerPresetNames.add(PresetEqualizer.NORMAL.getName());
        equalizerPresetNames.add(PresetEqualizer.CLASSIC.getName());
        equalizerPresetNames.add(PresetEqualizer.DANCE.getName());
        equalizerPresetNames.add(PresetEqualizer.FLAT.getName());
        equalizerPresetNames.add(PresetEqualizer.FOLK.getName());
        equalizerPresetNames.add(PresetEqualizer.HEAVY_METAL.getName());
        equalizerPresetNames.add(PresetEqualizer.HIP_HOP.getName());
        equalizerPresetNames.add(PresetEqualizer.JAZZ.getName());
        equalizerPresetNames.add(PresetEqualizer.POP.getName());
        equalizerPresetNames.add(PresetEqualizer.ROCK.getName());
        equalizerPresetSpinnerAdapter = new ArrayAdapter<>(getBaseActivity(),
                R.layout.spinner_item,
                equalizerPresetNames);
        equalizerPresetSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        thSpinner.setAdapter(equalizerPresetSpinnerAdapter);
        thSpinner.setOnItemSelectedListener(this);
    }

    private void onSetRoomSettingEqualizer(MessageBluetooth msg) {
        if (msg.getEqualizerPreset() != null && msg.getSeekBarEqualizerList() != null) {
            EqualizerTable equalizerTable = new EqualizerTable();
            equalizerTable.setId(msg.getEqualizerPreset().getId());
            equalizerTable.setThNameEqualizer(msg.getEqualizerPreset().getThNamePreset());
            equalizerTable.setSbFirst(msg.getSeekBarEqualizerList().get(0).getProgress());
            equalizerTable.setSbSecond(msg.getSeekBarEqualizerList().get(1).getProgress());
            equalizerTable.setSbThird(msg.getSeekBarEqualizerList().get(2).getProgress());
            equalizerTable.setSbFourth(msg.getSeekBarEqualizerList().get(3).getProgress());
            equalizerTable.setSbFifth(msg.getSeekBarEqualizerList().get(4).getProgress());
            roomViewModel.getRepository().insertRxSettingEqualizer(equalizerTable);
        }
    }

    private void onSetSeekBarEqualizerSpinner(MessageBluetooth msg) {

        if (msg.getSettingsSeekBars() != null) {
            for (SeekBar sb : seekBarEqualizers) {
                sb.setMax(msg.getSettingsSeekBars().getUpperEqualizerBandLevel() - msg.getSettingsSeekBars().getLowerEqualizerBandLevel());
                sb.setProgress(msg.getSettingsSeekBars().getSettingSeekBarEqualizers().get(seekBarEqualizers.indexOf(sb)).getThProgress());
                String i = String.valueOf(msg.getSettingsSeekBars().getSettingSeekBarEqualizers().get(seekBarEqualizers.indexOf(sb)).getThProgress() - 1500) + getString(R.string.db);
                txvDecibel.get(seekBarEqualizers.indexOf(sb)).setText(i);
            }
            for (TextView tv : txvHz) {
                tv.setText(SettingSeekBar.frequencyHeaderText(msg.getSettingsSeekBars().getSettingSeekBarEqualizers().get(txvHz.indexOf(tv)).getCenterFrequencyBand()));
            }
        }
        if (msg.getSeekBarEqualizerList() != null) {
            for (SeekBarEqualizer sbe : msg.getSeekBarEqualizerList()) {
                seekBarEqualizers.get(msg.getSeekBarEqualizerList().indexOf(sbe)).setProgress(msg.getSeekBarEqualizerList().get(msg.getSeekBarEqualizerList().indexOf(sbe)).getProgress());
                String i = String.valueOf(msg.getSeekBarEqualizerList().get(msg.getSeekBarEqualizerList().indexOf(sbe)).getProgress() - 1500) + getString(R.string.db);
                txvDecibel.get(msg.getSeekBarEqualizerList().indexOf(sbe)).setText(i);
            }
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        msgBL = new MessageBluetooth();
        if (equalizerPresetNames.size() > 0)
            msgBL.setEqualizerPreset(new EqualizerPreset(position - 1, equalizerPresetNames.get(position)));
        onSetSeekBarEqualizerPreset(getSettingEqualizerPreset(equalizerPresetNames.get(position)));
        sendMessage(msgBL);
        Toast.makeText(getBaseActivity(), position + "" + equalizerPresetNames.get(position), Toast.LENGTH_SHORT).show();
    }

    private PresetEqualizer getSettingEqualizerPreset(String namePreset) {
        if (namePreset.equals(NORMAL.getName())) return NORMAL;
        if (namePreset.equals(CLASSIC.getName())) return CLASSIC;
        if (namePreset.equals(DANCE.getName())) return DANCE;
        if (namePreset.equals(FLAT.getName())) return FLAT;
        if (namePreset.equals(FOLK.getName())) return FOLK;
        if (namePreset.equals(HEAVY_METAL.getName())) return HEAVY_METAL;
        if (namePreset.equals(HIP_HOP.getName())) return HIP_HOP;
        if (namePreset.equals(JAZZ.getName())) return JAZZ;
        if (namePreset.equals(POP.getName())) return POP;
        if (namePreset.equals(ROCK.getName())) return ROCK;
        return null;
    }

    private void onSetSeekBarEqualizerPreset(PresetEqualizer pe) {
        if (pe != null) {
            seekBarEqualizers.get(0).setProgress(pe.getSbFirst());
            seekBarEqualizers.get(1).setProgress(pe.getSbSecond());
            seekBarEqualizers.get(2).setProgress(pe.getSbThird());
            seekBarEqualizers.get(3).setProgress(pe.getSbFourth());
            seekBarEqualizers.get(4).setProgress(pe.getSbFifth());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void sendMessage(MessageBluetooth msgBL) {
        if (mModuleControllerBluetooth != null && mModuleControllerBluetooth.sendMessage(gson.toJson(msgBL)) != BluetoothModuleController.STATE_CONNECTED)
            Toast.makeText(getBaseActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
    }
}
