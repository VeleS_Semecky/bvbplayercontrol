package com.example.yurii.bluetoothexoplayer.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;

/**
 * This thread runs while attempting to make an outgoing connection
 * with a device. It runs straight through; the connection either
 * succeeds or fails.
 */
public class BluetoothConnectThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private final BluetoothAdapter mAdapter;
    private final BluetoothModuleController bluetoothModuleController;
    private static final String TAG = "BluetoothConnect";

    public BluetoothConnectThread(BluetoothDevice device, BluetoothAdapter mAdapter, BluetoothModuleController bluetoothModuleController) {
        this.mAdapter = mAdapter;
        mmDevice = device;
        this.bluetoothModuleController = bluetoothModuleController;
        BluetoothSocket tmp = null;
        // Get a BluetoothSocket for a connection with the
        // given BluetoothDevice
        try {
            tmp = device.createRfcommSocketToServiceRecord(BluetoothModuleController.MY_UUID);
        } catch (IOException e) {
            Log.e(TAG, "BluetoothConnectThread", e.fillInStackTrace());

        }
        mmSocket = tmp;
    }

    public void run() {
        setName("BluetoothConnectThread");
        // Always cancel discovery because it will slow down a connection
        mAdapter.cancelDiscovery();
        // Make a connection to the BluetoothSocket
        try {
            // This is a blocking call and will only return on a
            // successful connection or an exception
            mmSocket.connect();
        } catch (IOException e) {
            bluetoothModuleController.connectionFailed();
            // Close the socket
            try {
                mmSocket.close();
            } catch (IOException e2) {
                Log.e(TAG, "run", e2.fillInStackTrace());

            }
            // Start the service over to restart listening mode
            bluetoothModuleController.start();
            return;
        }
        // Reset the ConnectThread because we're done
        synchronized (bluetoothModuleController) {
            bluetoothModuleController.setConnectThread(null);
        }
        // Start the connected thread
        bluetoothModuleController.connected(mmSocket, mmDevice);
    }

    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "cancel", e.fillInStackTrace());
        }
    }
}