package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageBluetooth {
    @SerializedName("ctr")
    @Expose
    private
    ControllerMusic controllerMusic;

    @SerializedName("on_off")
    @Expose
    private
    ButtonMusicOnOrOff musicOnOrOff;

    @SerializedName("sb")
    @Expose
    private
    SeekBarEqualizer seekBarEqualizer;

    @SerializedName("sbl")
    @Expose
    private
    List<SeekBarEqualizer> seekBarEqualizerList;

    @SerializedName("prt")
    @Expose
    private
    EqualizerPreset equalizerPreset;

    @SerializedName("bass")
    @Expose
    private
    MessageBass messageBass;

    @SerializedName("rvrb")
    @Expose
    private
    MessageReverberation messageReverberation;

    //Setting
    @SerializedName("stgPrt")
    @Expose
    private
    List<EqualizerPreset> settingsPreset;
    @SerializedName("stgSB")
    @Expose
    private
    SettingSeekBar settingsSeekBars;
    @SerializedName("stgVol")
    @Expose
    private
    SettingVolume settingsVolume;

    public MessageBluetooth() {
    }

    public MessageBluetooth(ControllerMusic controllerMusic, ButtonMusicOnOrOff musicOnOrOff, SeekBarEqualizer seekBarEqualizer, List<SeekBarEqualizer> seekBarEqualizerList, EqualizerPreset equalizerPreset, MessageBass messageBass, MessageReverberation messageReverberation, List<EqualizerPreset> settingsPreset, SettingSeekBar settingsSeekBars, SettingVolume settingsVolume) {
        this.controllerMusic = controllerMusic;
        this.musicOnOrOff = musicOnOrOff;
        this.seekBarEqualizer = seekBarEqualizer;
        this.seekBarEqualizerList = seekBarEqualizerList;
        this.equalizerPreset = equalizerPreset;
        this.messageBass = messageBass;
        this.messageReverberation = messageReverberation;
        this.settingsPreset = settingsPreset;
        this.settingsSeekBars = settingsSeekBars;
        this.settingsVolume = settingsVolume;
    }

    public ControllerMusic getControllerMusic() {
        return controllerMusic;
    }

    public void setControllerMusic(ControllerMusic controllerMusic) {
        this.controllerMusic = controllerMusic;
    }

    public ButtonMusicOnOrOff getMusicOnOrOff() {
        return musicOnOrOff;
    }

    public void setMusicOnOrOff(ButtonMusicOnOrOff musicOnOrOff) {
        this.musicOnOrOff = musicOnOrOff;
    }

    public SeekBarEqualizer getSeekBarEqualizer() {
        return seekBarEqualizer;
    }

    public void setSeekBarEqualizer(SeekBarEqualizer seekBarEqualizer) {
        this.seekBarEqualizer = seekBarEqualizer;
    }

    public List<SeekBarEqualizer> getSeekBarEqualizerList() {
        return seekBarEqualizerList;
    }

    public void setSeekBarEqualizerList(List<SeekBarEqualizer> seekBarEqualizerList) {
        this.seekBarEqualizerList = seekBarEqualizerList;
    }

    public EqualizerPreset getEqualizerPreset() {
        return equalizerPreset;
    }

    public void setEqualizerPreset(EqualizerPreset equalizerPreset) {
        this.equalizerPreset = equalizerPreset;
    }

    public List<EqualizerPreset> getSettingsPreset() {
        return settingsPreset;
    }

    public void setSettingsPreset(List<EqualizerPreset> settingsPreset) {
        this.settingsPreset = settingsPreset;
    }

    public SettingSeekBar getSettingsSeekBars() {
        return settingsSeekBars;
    }

    public void setSettingsSeekBars(SettingSeekBar settingsSeekBars) {
        this.settingsSeekBars = settingsSeekBars;
    }

    public SettingVolume getSettingsVolume() {
        return settingsVolume;
    }

    public void setSettingsVolume(SettingVolume settingsVolume) {
        this.settingsVolume = settingsVolume;
    }

    public MessageBass getMessageBass() {
        return messageBass;
    }

    public void setMessageBass(MessageBass messageBass) {
        this.messageBass = messageBass;
    }

    public MessageReverberation getMessageReverberation() {
        return messageReverberation;
    }

    public void setMessageReverberation(MessageReverberation messageReverberation) {
        this.messageReverberation = messageReverberation;
    }
}
