package com.example.yurii.bluetoothexoplayer.api.presenter;


import com.example.yurii.bluetoothexoplayer.api.core.BaseView;
import com.example.yurii.bluetoothexoplayer.api.model.Song;
import com.example.yurii.bluetoothexoplayer.api.networking.NetworkSong;
import com.example.yurii.bluetoothexoplayer.api.networking.core.Service;
import com.example.yurii.bluetoothexoplayer.api.networking.error.NetworkError;
import com.example.yurii.bluetoothexoplayer.api.presenter.core.Presenter;

public class SongPresenter extends Presenter<NetworkSong,Song> {
    public SongPresenter(NetworkSong service, BaseView<Song> songBaseView) {
        super(service, songBaseView);
    }

    public void getSong(){
        yBaseView.showWait();
        subscription.add(service.getSong(new Service.Callback<Song>() {
            @Override
            public void onSuccess(Song song) {
                yBaseView.closeWait();
                yBaseView.onSuccess(song);
                onStop();
            }

            @Override
            public void onError(NetworkError networkError) {
                yBaseView.closeWait();
                yBaseView.onFailed(networkError.getAppErrorMessage());//
                onStop();
            }
        }));
        super.subscription.add(subscription);
    }
    public void setSong(String song){
        yBaseView.showWait();
        subscription.add(service.setSong(song ,new Service.Callback<Song>() {
            @Override
            public void onSuccess(Song song) {
                yBaseView.closeWait();
                yBaseView.onSuccess(song);
                onStop();
            }

            @Override
            public void onError(NetworkError networkError) {
                yBaseView.closeWait();
                yBaseView.onFailed(networkError.getAppErrorMessage());//
                onStop();
            }
        }));
        super.subscription.add(subscription);
    }
}
