package com.example.yurii.bluetoothexoplayer.helper;

import android.view.View;

public class HelperLayout {
    private View thView;
    private View helpView;
    private HelperString helperString;

    public HelperLayout() {
    }

    public HelperLayout(View thView, View helpView, HelperString helperString) {
        this.thView = thView;
        this.helpView = helpView;
        this.helperString = helperString;
    }

    public View getThView() {
        return thView;
    }

    public void setThView(View thView) {
        this.thView = thView;
    }

    public View getHelpView() {
        return helpView;
    }

    public void setHelpView(View helpView) {
        this.helpView = helpView;
    }

    public HelperString getHelperString() {
        return helperString;
    }

    public void setHelperString(HelperString helperString) {
        this.helperString = helperString;
    }
}
