package com.example.yurii.bluetoothexoplayer.api.service;


import com.example.yurii.bluetoothexoplayer.api.model.SendSong;
import com.example.yurii.bluetoothexoplayer.api.model.Song;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SongApi {
    @GET("/song")
    Observable<Song> getSong();
    
    @FormUrlEncoded
    @POST("/song")
    Observable<Song> setSong(@Field("song") String yourField);

}
