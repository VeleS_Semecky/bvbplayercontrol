package com.example.yurii.bluetoothexoplayer.bluetooth.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingSeekBarEqualizer {

    @SerializedName("id")
    @Expose
    private short id;

    //  center frequency of the given band.
    @SerializedName("frq")
    @Expose
    private int centerFrequencyBand;

    @SerializedName("prg")
    @Expose
    private int thProgress;

    public SettingSeekBarEqualizer() {
    }

    public SettingSeekBarEqualizer(short id, int centerFrequencyBand, int thProgress) {
        this.id = id;
        this.centerFrequencyBand = centerFrequencyBand;
        this.thProgress = thProgress;
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public int getCenterFrequencyBand() {
        return centerFrequencyBand;
    }

    public void setCenterFrequencyBand(int centerFrequencyBand) {
        this.centerFrequencyBand = centerFrequencyBand;
    }

    public int getThProgress() {
        return thProgress;
    }

    public void setThProgress(int thProgress) {
        this.thProgress = thProgress;
    }
}
