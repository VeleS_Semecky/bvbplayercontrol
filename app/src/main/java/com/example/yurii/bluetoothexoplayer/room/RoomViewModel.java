package com.example.yurii.bluetoothexoplayer.room;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

public class RoomViewModel extends AndroidViewModel {
    private Repository mRepository;


    public RoomViewModel(Application application) {
        super(application);
        mRepository = new Repository(application);
    }

    public Repository getRepository() {
        return mRepository;
    }


    @Override
    protected void onCleared() {
        mRepository.unsubscribeRxJava();
        super.onCleared();
    }
}
