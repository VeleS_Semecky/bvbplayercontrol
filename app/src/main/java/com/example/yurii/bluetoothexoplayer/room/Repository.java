package com.example.yurii.bluetoothexoplayer.room;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.util.Log;

import com.example.yurii.bluetoothexoplayer.room.dao.EqualizerDAO;
import com.example.yurii.bluetoothexoplayer.room.dao.core.RoomDatabase;
import com.example.yurii.bluetoothexoplayer.room.model.EqualizerTable;


import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Repository {

    private EqualizerDAO mEqualizerDAO;

    private LiveData<List<EqualizerTable>> mAllSettingEqualizer;

    private final CompositeDisposable mDisposable = new CompositeDisposable();

    public CompositeDisposable getDisposable() {
        return mDisposable;
    }

    Repository(Context application) {
        RoomDatabase db = RoomDatabase.getDatabase(application);
        mEqualizerDAO = db.equalizerDAO();
        mAllSettingEqualizer = mEqualizerDAO.getAllSettingEqualizer();
    }


    public LiveData<List<EqualizerTable>> getAllSettingEqualizer() {
        return mAllSettingEqualizer;
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public Observable getO(EqualizerTable track){
        return Observable.fromCallable(() -> mEqualizerDAO.insertSettingEqualizer(track));
    }
    private Completable updateListSettingEqualizer(List<EqualizerTable> mEqualizers) {
        return Completable.fromAction(() -> { mEqualizerDAO.insertAllSettingEqualizer(mEqualizers);

        });
    }

    private Completable updateSettingEqualizer(EqualizerTable mEqualizer) {

        return Completable.fromAction(() -> {
            mEqualizerDAO.insertSettingEqualizer(mEqualizer);
        });
    }



    public void insertRxListSettingEqualizer(List<EqualizerTable> mEqualizers) {
        mDisposable.add(updateListSettingEqualizer(mEqualizers)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Log.e("Log", "Update TrackTable"),
                        throwable -> Log.e("Log", "Unable to update TrackTable", throwable)));

    }

    public void insertRxSettingEqualizer(EqualizerTable mEqualizer) {
        mDisposable.add(updateSettingEqualizer(mEqualizer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Log.e("Log", "Update TrackTable"),
                        throwable -> Log.e("Log", "Unable to update TrackTable", throwable)));

    }

    public void unsubscribeRxJava(){
        if(!mDisposable.isDisposed()) {
            mDisposable.dispose();
        }
    }
}
