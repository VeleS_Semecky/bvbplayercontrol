package com.example.yurii.bluetoothexoplayer.api.bean.core;


import android.content.Context;

import com.example.yurii.bluetoothexoplayer.api.core.BaseView;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

@EBean
public abstract class Bean<T> implements BaseView<T> {
    @RootContext
    public Context baseActivity;


    @Override
    public void onFailed(String error) {

    }

    @Override
    public void onSuccess(T t) {

    }

    @Override
    public void showWait() {

    }

    @Override
    public void closeWait() {

    }
}
